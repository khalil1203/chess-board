const board = document.querySelector('.chess-board');
function createDiv() {
    return document.createElement('div');
}

for (let i = 0; i < 8; i++) {
    let row = createDiv();
    row.classList.add('row');
    setRowDimensions(row);
    for (let j = 0; j < 8; j++) {
        let box = createDiv();
        row.appendChild(box);
        box.dataset.row = i;
        box.dataset.col = j;
        setBoxDimensions(box);
        box.classList.add('box');
        console.log(box.dataset.row, box.dataset.col);
    }
    board.appendChild(row);
}


function setBoxDimensions(box) {
    box.style.width = "100px";
    box.style.height = "100px";
    if (box.dataset.col % 2 == 0 && box.dataset.row % 2 == 0) {
        box.style.backgroundColor = "white";
    }
    if (box.dataset.col % 2 == 0 && box.dataset.row % 2 != 0) {
        box.style.backgroundColor = "black";
    }
    if (box.dataset.col % 2 != 0 && box.dataset.row % 2 != 0) {
        box.style.backgroundColor = "white";
    }
    if (box.dataset.col % 2 != 0 && box.dataset.row % 2 == 0) {
        box.style.backgroundColor = "black";
    }
}

function setRowDimensions(row) {
    row.style.width = "800px";
    row.style.height = "100px";
}



board.addEventListener('click', changeColor);


function changeColor() {
    let currentBox = event.target;
    console.log(currentBox);
    let row = currentBox.dataset.row;
    let col = currentBox.dataset.col;

    let check = [[false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],];

    changeDiagonal(0, 0, row, col, check);

}



// function changeAllRed(row, col, currentBox) {
//     if (row < 0 || col < 0 || row > 7 || col > 7 || check[row][col]) {
//         return;
//     } else {
//         check[row][col] = true;

//         // Get the specific element at the given row and column coordinates
//         const current = document.querySelector(`[data-row="${row}"][data-col="${col}"]`);
//         console.log(current);
//         current.style.backgroundColor = "red";

//         changeRed(row - 1, col - 1,currentBox);
//         changeRed(row - 1, col + 1,currentBox);
//         changeRed(row + 1, col - 1,currentBox);
//         changeRed(row + 1, col + 1,currentBox);
//     }
// }

function changeDiagonal(row, col, parentRow, parentCol, check) {

    if (row < 0 || row >= 8 || col < 0 || col >= 8 || check[row][col]) {
        return;
    }

    check[row][col] = true;

    if (Math.abs(row - parentRow) === Math.abs(col - parentCol)) {
        const current = document.querySelector(`[data-row="${row}"][data-col="${col}"]`);
        current.style.backgroundColor = "red";
    }

    changeDiagonal(row - 1, col - 1, parentRow, parentCol, check);
    changeDiagonal(row - 1, col + 1, parentRow, parentCol, check);
    changeDiagonal(row + 1, col - 1, parentRow, parentCol, check);
    changeDiagonal(row + 1, col + 1, parentRow, parentCol, check);
}